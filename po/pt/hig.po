#
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-17 01:43+0000\n"
"PO-Revision-Date: 2023-01-18 13:29+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: HIG\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "Guias de Interfaces Humanas do KDE"

#: content/hig/_index.md:20
msgid ""
"Welcome to the KDE Human Interface Guidelines! Following them will help you "
"build a beautiful and powerful app that feels at home when run in [KDE "
"Plasma](https://kde.org/plasma-desktop), and works well outside of it. Users "
"will learn how to use your app quickly, accomplish their goals with ease, "
"and encounter fewer issues requiring support."
msgstr ""

#: content/hig/_index.md:22
msgid ""
"This document covers KDE's design philosophy and culture, common workflows "
"and patterns, standard user interface conventions, and recommendations for "
"platform integration."
msgstr ""

#: content/hig/_index.md:25
msgid "Why is good design so important?"
msgstr ""

#: content/hig/_index.md:26
msgid ""
"Design determines not only how well something fulfills its functional "
"purpose, but also how enjoyable it is to interact with. If either is "
"lacking, the result will be frustrating. People won't want to use a poorly-"
"designed app, and if forced to, they'll be resentful and spread negativity "
"about it. Avoid this through good design!"
msgstr ""

#: content/hig/_index.md:28
msgid ""
"Design involves targeted decisions and trade-offs that bring a project "
"closer to its intended goals and usage paradigms. The best apps know which "
"users and use cases they're targeting, and which ones they leave for others. "
"It's better to succeed by focusing on what you can do well than to spread "
"yourself thin and fail. Under-promise and over-deliver, never the reverse."
msgstr ""

#: content/hig/_index.md:31
msgid "Topics not covered"
msgstr ""

#: content/hig/_index.md:32
msgid ""
"This document is a set of design guidelines, not instructions for "
"implementing every specific component. While some technical guidance will be "
"provided, much more detail can be found in the components' own [usage]"
"(https://develop.kde.org/docs/) and [API](https://api.kde.org/) "
"documentation as well as the [source code of existing KDE apps](https://"
"invent.kde.org/explore/groups?sort=name_asc). It's more of a *what* and a "
"*why*, not a *how*."
msgstr ""

#: content/hig/_index.md:34
msgid ""
"This document is not intended to be an ironclad law code. By learning and "
"following the rules, you'll understand how to safely innovate within the "
"guidelines, and when it can be appropriate to break them if it produces a "
"superior result."
msgstr ""

#: content/hig/_index.md:37
msgid "Contributing"
msgstr ""

#: content/hig/_index.md:38
msgid ""
"This is a living document, intended to be updated over time to reflect "
"current KDE design trends and best practices. Contributions are welcome!"
msgstr ""

#: content/hig/_index.md:40
msgid ""
"Content is written in [Markdown](https://commonmark.org/help/), and the "
"source files are hosted in a [Git repository](https://invent.kde.org/"
"documentation/develop-kde-org.git). To quickly edit a page, the GitLab "
"editor can be opened on any page with the “Edit this page” button in the "
"drawer on the right. Tasks and changes are organized via https://invent.kde."
"org. To learn how to submit changes, see the [README file](https://invent."
"kde.org/documentation/develop-kde-org/-/blob/master/README.md)."
msgstr ""

#: content/hig/_index.md:43
msgid "Read the guidelines"
msgstr ""

#: content/hig/_index.md:44
msgid "Click on any of these tiles to jump to a page:"
msgstr ""

#~ msgid ""
#~ "The **KDE Human Interface Guidelines (HIG)** offer designers and "
#~ "developers a set of recommendations for producing beautiful, usable, and "
#~ "consistent user interfaces for convergent desktop and mobile applications "
#~ "and workspace widgets. Our aim is to improve the experience for users by "
#~ "making consistent interfaces for desktop, mobile, and everything in "
#~ "between, more consistent, intuitive and learnable."
#~ msgstr ""
#~ "As **Linhas-Guia de Interfaces Humanas do KDE (HIG)** oferecem aos "
#~ "desenhadores e programadores um conjunto de recomendações para produzir "
#~ "interfaces de utilizador bonitas, úteis e consistentes nas aplicações "
#~ "convergentes para computadores e dispositivos móveis, bem como nos "
#~ "elementos gráficos do ambiente de trabalho. O seu objectivo é melhorar a "
#~ "experiência dos utilizadores, criando interfaces das aplicações e "
#~ "elementos mais consistentes e, deste modo, mais intuitivas e fáceis de "
#~ "aprender."

#~ msgid "Design Vision"
#~ msgstr "Visão do Desenho"

#~ msgid ""
#~ "Our design vision focuses on two attributes of KDE software that connect "
#~ "its future to its history:"
#~ msgstr ""
#~ "A visão do desenho geral foca-se em dois atributos das aplicações do KDE "
#~ "que interligam o seu futuro com o seu histórico:"

#~ msgid ""
#~ "![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
#~ "png)"
#~ msgstr ""
#~ "![Simples por omissão, poderoso quando necessário.](/hig/"
#~ "HIGDesignVisionFullBleed.png)"

#~ msgid "Simple by default..."
#~ msgstr "Simples por omissão..."

#~ msgid ""
#~ "*Simple and inviting. KDE software is pleasant to experience and easy to "
#~ "use.*"
#~ msgstr ""
#~ "*Simples e convidativo. As aplicações do KDE são agradáveis de interagir "
#~ "e fáceis de usar.*"

#~ msgid "**Make it easy to focus on what matters**"
#~ msgstr "**Facilitar o foco no que interessa**"

#~ msgid ""
#~ "Remove or minimize elements not crucial to the primary or main task. Use "
#~ "spacing to keep things organized. Use color to draw attention. Reveal "
#~ "additional information or optional functions only when needed."
#~ msgstr ""
#~ "Remover ou minimizar os elementos não cruciais para a tarefa principal. "
#~ "Use espaços de intervalo para manter as coisas organizadas. Use cores "
#~ "para chamar a atenção. Revele as informações adicionais ou funções "
#~ "opcionais só quando for necessário."

#~ msgid "**\"I know how to do that!\"**"
#~ msgstr "**\"Sei como fazê-lo!\"**"

#~ msgid ""
#~ "Make things easier to learn by reusing design patterns from other "
#~ "applications. Other applications that use good design are a precedent to "
#~ "follow."
#~ msgstr ""
#~ "Torne as coisas mais simples de aprender, reutilizando padrões de desenho "
#~ "de outras aplicações. As aplicações que usam um bom desenho são um "
#~ "precedente a seguir."

#~ msgid "**Do the heavy lifting for me**"
#~ msgstr "**Fazer o trabalho pesado por mim**"

#~ msgid ""
#~ "Make complex tasks simple. Make novices feel like experts. Create ways in "
#~ "which your users can naturally feel empowered by your software."
#~ msgstr ""
#~ "Torne simples as tarefas complexas. Faça com que os principiantes se "
#~ "sintam especialistas. Crie formas nas quais os utilizadores se sintam "
#~ "naturalmente reforçados pelo seu 'software'."

#~ msgid "...Powerful when needed"
#~ msgstr "...Poderoso quando necessário"

#~ msgid ""
#~ "*Power and flexibility. KDE software allows users to be effortlessly "
#~ "creative and efficiently productive.*"
#~ msgstr ""
#~ "*Poder e flexibilidade. As aplicações do KDE permitem aos utilizadores "
#~ "serem criativos sem esforço e produtivos de forma eficiente."

#~ msgid "**Solve a problem**"
#~ msgstr "**Resolver um problema**"

#~ msgid ""
#~ "Identify and make very clear to the user what need is addressed and how."
#~ msgstr ""
#~ "Identifique e torne bastante claro ao utilizador a necessidade que é "
#~ "tratada e de que forma é feito."

#~ msgid "**Always in control**"
#~ msgstr "**Sempre no controlo**"

#~ msgid ""
#~ "It should always be clear what can be done, what is currently happening, "
#~ "and what has just happened. The user should never feel at the mercy of "
#~ "the tool. Give the user the final say."
#~ msgstr ""
#~ "Deverá ser sempre claro o que poderá ser feito, o que está a acontecer no "
#~ "preciso momento e o que acabou de acontecer. O utilizador nunca se deve "
#~ "sentir à mercê da ferramenta. Dê ao utilizador a última palavra."

#~ msgid "**Be flexible**"
#~ msgstr "**Ser flexível**"

#~ msgid ""
#~ "Provide sensible defaults but consider optional functionality and "
#~ "customization options that don\\'t interfere with the primary task."
#~ msgstr ""
#~ "Forneça opções predefinidas válidas, mas tenha em consideração as "
#~ "funcionalidades opcionais e as opções de personalização que não "
#~ "interfiram com a tarefa principal."

#~ msgid "Note"
#~ msgstr "Nota"

#~ msgid ""
#~ "KDE encourages developing and designing for customization, while "
#~ "providing good default settings. Integrating into other desktop "
#~ "environments is also a virtue, but ultimately we aim for perfection "
#~ "within our own Plasma desktop environment with the default themes and "
#~ "settings. This aim should not be compromised."
#~ msgstr ""
#~ "O KDE encoraja o desenvolvimento e a concepção de personalizações, "
#~ "fornecendo por outro lado boas opções predefinidas. A integração noutros "
#~ "ambientes de trabalho também é uma virtude mas, em última instância, deve "
#~ "atingir a perfeição dentro do nosso ambiente de trabalho Plasma com os "
#~ "temas e opções predefinidas. Este objectivo não deverá ser comprometido."
